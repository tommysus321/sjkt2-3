var express = require("express");
var router = express.Router();
const app = require("../app");
const { ensureAuthenticated } = require("../controllers/auth")

/* GET home page. */
/*router.get("/", function (req, res, next) {
  res.redirect("/pegawai", ensureAuthenticated, (req, res) => res.render("pegawai", {
    name: req.user.name
  }));
}); */

router.get('/', (req, res) => res.render('welcome'));
// Dashboard
router.get('/pegawai', ensureAuthenticated, (req, res) => res.render('pegawai', {
    name: req.user.name
}));

module.exports = router;

// membuat variable router dengan require atau export variabel express
// Dan menggunakan metode Router
const router = require("express").Router();
// export controller yang ingin dipakai
const pegawaiController = require("../controllers/pegawaiController");
// const eksporController = require("../controllers/eksporController");

// endpoint pegawai
router.get("/", pegawaiController.viewPegawai); // Untuk view
router.post("/", pegawaiController.addPegawai); // Untuk menambahkan data pegawai
router.put("/", pegawaiController.editPegawai); // Untuk edit data pegawai
router.delete("/:id", pegawaiController.deletePegawai); // Untuk hapus data pegawai berdasarkan id

// Lalu export routernya
module.exports = router;

const mongoose = require("mongoose");

// Membuat variabel baru dengan nama pegawaiScheme
const pegawaiScheme = new mongoose.Schema({
  nama: {
    // Membuat type dari field nama yang berada di tabel pegawai bersifat string
    type: String,
    // maksud dari required adalah ketika data disimpan kedalam database, data tidak boleh kosong
    required: true,
  },
  nomor_wa: {
    // Membuat type dari field nama yang berada di tabel pegawai bersifat number
    type: String,
    required: true,
  },
  tanggal_pkt: {
    type: Date,
    required: true,
  },
});

const usersScheme = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date
});

module.exports = mongoose.model("Users", usersScheme);
module.exports = mongoose.model("Pegawai", pegawaiScheme);
